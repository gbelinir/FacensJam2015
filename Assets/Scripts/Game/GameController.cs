﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Game;

public class GameController : MonoBehaviour {

    [HideInInspector]
    public Player m_player;
    private DogBehaviour m_dog;
    private CameraBehaviour m_camera;
    private ChatController m_chatController;
    private HudController m_hudController;
    private SDFXController m_sdfxController;
    private AudioSource m_audioSource;
    public GameState m_gameState;
    public Transform PosToDog;
    public TileBehaviour PosDogComeco;
    public Transform StartPos;
    public TileBehaviour TileStart;
    public Animator MorreProMonstroAnim;
    public Transform PosForPlayer;
    public Animator FinalBom;
    public Animator FinalRuim;
    public GameObject ModalEnd;
    public AudioClip[] Audios;

    public enum GameState
    {
        None,
        Start,
        Running,
        Chat
    }


	void Start () {
        m_gameState = GameState.Start;
        m_player = FindObjectOfType<Player>();
        m_dog = FindObjectOfType<DogBehaviour>();
        m_camera = FindObjectOfType<CameraBehaviour>();
        m_chatController = FindObjectOfType<ChatController>();
        m_hudController = FindObjectOfType<HudController>();
        m_sdfxController = FindObjectOfType<SDFXController>();
        m_audioSource = GetComponent<AudioSource>();
        m_audioSource.clip = Audios[0];
        m_audioSource.Play();
        StartingGame();
	}

    private void StartingGame()
    {
        m_gameState = GameState.Chat;
        m_chatController.ShowChat("Precisamos sair daqui amigão!", () =>
        {
            m_dog.Walk(true);
            m_dog.MoveTo(PosDogComeco, null);
            m_chatController.ShowChat("Hey espera por mim! Onde vc vai?", () =>
            {
                m_chatController.ShowChat("Budddddd!", () =>
                {
                    m_chatController.ShowChat("Droga, onde será que ele foi?", () =>
                    {
                        m_chatController.ShowChat("Preciso encontrá-lo!", () =>
                        {
                            m_dog.Walk(false);
                            m_dog.StopMoving();
                            m_dog.CaoPreso();
                            m_dog.transform.position = new Vector3(PosToDog.transform.position.x, PosToDog.transform.position.y, PosToDog.transform.position.z);
                            m_gameState = GameState.Running;
                        });
                    });
                });
            });
        });
    }

    private void CaoPreso(TileBehaviour NextTile)
    {
         m_gameState = GameState.Chat;
         m_chatController.ShowChat("Amigão, que bom te achei!", () =>
         {
             m_chatController.ShowChat("Como vc se enfiou aí?! Deixa eu te ajudar!", () =>
             {
                 m_dog.CaoLibertado();
                 Invoke("AdjustPosDog", 1f);
                 NextTile.DogIsThere = false;
             });
         });
    }

    private void AdjustPosDog()
    {
        m_dog.transform.parent = m_player.transform;
        Vector3 current = new Vector3(1,1,1);
        m_dog.transform.localScale = current;
        m_dog.transform.localPosition = new Vector3(PosForPlayer.transform.localPosition.x, PosForPlayer.transform.localPosition.y, m_dog.transform.position.z);
        m_gameState = GameState.Running;
    }

    private void EndGame()
    {
        m_gameState = GameState.Chat;
        m_chatController.ShowChat("O que foi isso Buddy?!", () =>
           {
               m_chatController.ShowChat("Droga estou sem balas", () =>
               {
                   m_chatController.ShowChat("Buddy NÃOOOOO!", () =>
                   {
                       ModalEnd.SetActive(true);
                   });
               });
           });
    }

    public void OnDoorClick(TileBehaviour NextTile)
    {
        m_gameState = GameState.Chat;
        if (NextTile.TileSound != null)   
        {
            m_audioSource.clip = NextTile.TileSound;
            m_audioSource.Play();    
        }

        m_camera.MoveTo(NextTile.transform.localPosition, () =>
        {
            m_player.Walk(true);
            if (m_player.HaveDog)
                m_dog.Walk(true);

            m_player.MoveTo(NextTile, () =>
                {
                    
            m_gameState = GameState.Running;
                    m_player.Walk(false);
                    if (NextTile.DogIsThere)
                    {
                        NextTile.DiscoverAnimation();
                        CaoPreso(NextTile);
                    }
                    else if (NextTile.End)
                    {
                        NextTile.DiscoverAnimation();
                        if (m_player.HaveDog)
                            m_dog.Walk(false);
                        EndGame();
                    }
                    else
                    {
                        if (m_player.HaveDog)
                            m_dog.Walk(false);

                        if (!NextTile._isDiscovered)
                        {
                            NextTile.DiscoverAnimation();

                            if (NextTile.Messages.Length == 1)
                            {
                                m_gameState = GameState.Chat;
                                m_chatController.ShowChat(NextTile.Messages[0], () =>
                                {
                                    Monster(NextTile);
                                });
                            }
                            else if (NextTile.Messages.Length == 2)
                            {
                                m_gameState = GameState.Chat;
                                m_chatController.ShowChat(NextTile.Messages[0], () =>
                                {
                                    m_chatController.ShowChat(NextTile.Messages[1], () =>
                                    {
                                        Monster(NextTile);
                                    });
                                });
                            }
                            else
                            {
                                Monster(NextTile);
                            }
                        }
                        else
                        {
                            Monster(NextTile);
                        }
                    }
                });
        });
    }

    private void Monster(TileBehaviour NextTile)
    {
        if(NextTile.Monster!= null)
        {
            m_gameState = GameState.Chat;
            if (m_player.HaveDog)
                m_dog.CaoEncontraMonstro();

            m_player.AchandoMonstro();

            m_sdfxController.PlayMonsterAttackk();
            NextTile.Monster.ShowAnimation(() =>
                {
                    Invoke("AnimDie", 1f);
                    Invoke("Die", 2f);
                });
        }

        Key(NextTile);
    }

    private void AnimDie()
    {
        m_sdfxController.PlayBoyMorrendoo();
        MorreProMonstroAnim.gameObject.SetActive(true);
    }

    private void Die()
    {
        m_sdfxController.PlayDeathh();
        m_hudController.DeathsCount++;
        m_hudController.RefreshHud();
        m_player.m_actualTile = TileStart;
        m_camera.transform.position = new Vector3(StartPos.position.x, StartPos.position.y, m_camera.transform.position.z);
        m_player.transform.position = new Vector3(StartPos.position.x, StartPos.position.y,m_player.transform.position.z);
        MorreProMonstroAnim.gameObject.SetActive(false);
        m_gameState = GameState.Running;
    }

    private void Key(TileBehaviour NextTile)
    {
        if (NextTile.HasKey != null)
        {
            m_gameState = GameState.Chat;
            m_player.AchandoItem();
            m_sdfxController.PlayItemChavee();
            NextTile.HasKey.ShowAnimation(() =>
            {
                m_gameState = GameState.Running;
                m_player.Keys++;
                m_hudController.KeysCount++;
                m_hudController.RefreshHud();
                GameObject.Destroy(NextTile.HasKey.gameObject);
            });
        }

        Memory(NextTile);
    }

    private void Memory(TileBehaviour NextTile)
    {
        if (NextTile.Memory != null && !NextTile.Memory.DiscoveredMemory)
        {
            m_gameState = GameState.Chat;
            NextTile.Memory.DiscoveredMemory = true;
            NextTile.Memory.gameObject.SetActive(true);
            NextTile.Memory.ShowAnimation(() =>
            {
                m_gameState = GameState.Running;
                NextTile.Memory.gameObject.SetActive(false);
            });
        }
        else
          m_gameState = GameState.Running;
    }


    public void EndGame(bool Good) 
    {
        m_audioSource.clip = Audios[1];
        m_audioSource.Play();
        if (Good)
        {
            FinalBom.gameObject.SetActive(true);
        }
        else
        {
            FinalRuim.gameObject.SetActive(true);
        }
    }


    public void Final()
    {
        Application.LoadLevel("Fim");
    }

}
