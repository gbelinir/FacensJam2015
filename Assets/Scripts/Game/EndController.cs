﻿using UnityEngine;
using System.Collections;

public class EndController : MonoBehaviour {

    public void OnDeixar()
    {
        gameObject.SetActive(false);
        GameObject.FindObjectOfType<GameController>().EndGame(true);
    }

    public void OnFicar()
    {
        gameObject.SetActive(false);
        GameObject.FindObjectOfType<GameController>().EndGame(false);
    }
}
