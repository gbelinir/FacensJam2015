﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Game;

public class TileBehaviour : MonoBehaviour {

    public MonsterBehaviour Monster;
    public Sprite[] DiscoveredTile;
    public string[] Messages;
    public KeyBehaviour HasKey;
    public MemoryBehaviour Memory;
    public GameObject tile;
    public bool End;
    public AudioClip TileSound;

    public bool DogIsThere;
    public bool _isDiscovered;
    private SpriteRenderer m_sprite;
    
    void Start()
    {
        m_sprite = GetComponent<SpriteRenderer>();
    }

    /// <summary>
    /// Occurs when player enter in this Tile
    /// </summary>
    public void DiscoverAnimation()
    {
            _isDiscovered = true;
            //AnimationTile

            //ChangingSprite
            m_sprite.sprite = DiscoveredTile[Random.Range(0, DiscoveredTile.Length - 1)];
            tile.SetActive(false);
    }

}
