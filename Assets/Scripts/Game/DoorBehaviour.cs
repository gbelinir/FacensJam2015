﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Game;

public class DoorBehaviour : MonoBehaviour {

    public TileBehaviour NextTile;
    public bool NeedKey;

    private GameController m_gameController;

    void Start()
    {
        m_gameController = FindObjectOfType<GameController>();
    }

    void OnMouseDown()
    {
        if (GameObject.FindObjectOfType<GameController>().m_gameState == GameController.GameState.Running && !m_gameController.m_player.m_move)
        {
            if (NeedKey)
            {
                Player currentPlayer = GameObject.FindObjectOfType<Player>();
                if (currentPlayer.Keys > 0)
                {
                    GameObject.FindObjectOfType<GameController>().m_gameState = GameController.GameState.Chat;
                    NeedKey = false;
                    currentPlayer.Keys--;
                    m_gameController.OnDoorClick(NextTile);
                    GameObject.FindObjectOfType<SDFXController>().PlayPortaAbrindo();
                }
                else
                {
                    GameObject.FindObjectOfType<GameController>().m_gameState = GameController.GameState.Chat;
                    GameObject.FindObjectOfType<ChatController>().ShowChat("Parece que não tenho chave =(", () =>
                        {
                            GameObject.FindObjectOfType<GameController>().m_gameState = GameController.GameState.Running;
                        });
                }
            }
            else
            {
                GameObject.FindObjectOfType<GameController>().m_gameState = GameController.GameState.Chat;
                GameObject.FindObjectOfType<SDFXController>().PlayPortaAbrindo();
                m_gameController.OnDoorClick(NextTile);
            }
        }
    }
	
}
