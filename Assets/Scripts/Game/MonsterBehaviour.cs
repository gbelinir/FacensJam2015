﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class MonsterBehaviour : MonoBehaviour
    {
        Animator m_animator;
        void Start()
        {
            m_animator = GetComponent<Animator>();
        }
        /// <summary>
        /// Animation of monster appearing
        /// </summary>
        public void ShowAnimation(Action CallBack)
        {
            m_animator.SetTrigger("encontrado");
            CallBack.Invoke();
        }
    }
}
