﻿using UnityEngine;
using System.Collections;
using System;

public class DogBehaviour : MonoBehaviour {

    private bool m_move;
    private Vector3 m_target;
    private Action m_callback;
    private TileBehaviour m_actualTile;
    private bool m_justFlipOnce;
    public Animator m_animator;

    public float Speed = 5;

    void Update()
    {
        if (m_move)
        {
            Vector3 target = new Vector3(m_target.x, m_target.y, transform.position.z);

            if (transform.position.x - m_target.x > 0 && transform.localScale.x > 0 && !m_justFlipOnce)
            {
                m_justFlipOnce = true;
                Vector3 localScale = transform.localScale;
                localScale.x *= -1;
                transform.localScale = localScale;
            }
            else if (transform.position.x - m_target.x < 0 && transform.localScale.x < 0 && !m_justFlipOnce)
            {
                m_justFlipOnce = true;
                Vector3 localScale = transform.localScale;
                localScale.x *= -1;
                transform.localScale = localScale;
            }

            transform.localPosition = Vector3.Lerp(transform.position, target, 0.01f * Speed);

            if (Vector3.Distance(m_target, transform.localPosition) < 0.2f)
            {
                m_move = false;
                _onFinishMovement();
            }
        }
    }

    public void MoveTo(TileBehaviour tile, Action callback)
    {
        m_justFlipOnce = false;
        m_actualTile = tile;
        m_callback = callback;
        m_move = true;
        m_target = tile.transform.position;
    }

    public void StopMoving()
    {
        m_move = false;
    }

    void _onFinishMovement()
    {
        if (m_callback != null)
         m_callback.Invoke();
        //Verificar se aqui deve atualizar a tile atual (mostrar a tile, atualizar valores da tile etc...)
    }

    public void Walk(bool walk)
    {
        m_animator.SetBool("andando", walk);
    }

    public void CaoEncontraMonstro()
    {
        m_animator.SetTrigger("encontramonstro");
    }

    public void CaoLibertado()
    {
        m_animator.SetTrigger("caolibertado");
    }

    public void CaoPreso()
    {
        m_animator.SetTrigger("caopreso");
    }

}
