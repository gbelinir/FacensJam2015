﻿using UnityEngine;
using System.Collections;

public class SDFXController : MonoBehaviour {

    private AudioSource m_audioSource;

    public AudioClip PortaAbrindo;
    public AudioClip PlayBoyMorrendo;
    public AudioClip PlayDogMorrendo;
    public AudioClip PlayDogLatindo;
    public AudioClip PlayDogChorandoLeve;
    public AudioClip PlayMonsterAttack;
    public AudioClip PlayDogPreso;
    public AudioClip PlayItemChave;
    public AudioClip PlayDeath;

    void Start()
    {
        m_audioSource = GetComponent<AudioSource>();    
    }

    public void PlayPortaAbrindo()
    {
        m_audioSource.PlayOneShot(PortaAbrindo);
    }

    public void PlayBoyMorrendoo()
    {
        m_audioSource.PlayOneShot(PlayBoyMorrendo);
    }

    public void PlayDogMorrendoo()
    {
        m_audioSource.PlayOneShot(PlayDogMorrendo);
    }

    public void PlayDogLatindoo()
    {
        m_audioSource.PlayOneShot(PlayDogLatindo);
    }

    public void PlayDogChorandoLevee()
    {
        m_audioSource.PlayOneShot(PlayDogChorandoLeve);
    }

    public void PlayMonsterAttackk()
    {
        m_audioSource.PlayOneShot(PlayMonsterAttack);
    }

    public void PlayDogPresoo()
    {
        m_audioSource.PlayOneShot(PlayDogPreso);
    }


    public void PlayItemChavee()
    {
        m_audioSource.PlayOneShot(PlayItemChave);
    }

    public void PlayDeathh()
    {
        m_audioSource.PlayOneShot(PlayDeath);
    }
}
