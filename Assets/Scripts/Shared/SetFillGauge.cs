﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SetFillGauge : MonoBehaviour
{
    public float maxValue;
    public float nextValue;

    private float m_currentValue;

    void Start()
    {
        m_currentValue = nextValue;

        float percentage = getPercentage();

        transform.localScale = new Vector3(percentage, transform.localScale.y, transform.localScale.z);
    }

    void Update()
    {
        //Significa que os valores estao iguais ou muito proximos
        if (Mathf.Abs(maxValue - m_currentValue) < 0.1f)
            return;

        float percentage = getPercentage();

        transform.localScale = Vector3.Lerp(transform.localScale,
            new Vector3(percentage, transform.localScale.y, transform.localScale.z),
            Time.deltaTime);
    }

    public void UpdateValue()
    {
        m_currentValue = nextValue;
    }

    private float getPercentage()
    {
        return m_currentValue / maxValue;
    }
}
